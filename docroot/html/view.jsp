<%--
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/init.jsp" %>

This is the <strong>Sample UI Search Container Taglib Portlet</strong>. This was made to showcase Liferay's liferay-ui:search-container taglib.<br /><br />

<portlet:renderURL var="basic" windowState="MAXIMIZED">
  <portlet:param name="mvcPath" value="/html/taglib/basic.jsp" />
</portlet:renderURL>
<a href="<%=basic.toString()%>">Basic Example</a><br /><br /> 


<portlet:renderURL var="advanced" windowState="MAXIMIZED">
  <portlet:param name="mvcPath" value="/html/taglib/advanced.jsp" />
</portlet:renderURL>
<a href="<%=advanced.toString()%>">Advanced Example</a>

This is the
<strong>Sample UI Taglibs Portlet</strong>
. This was made to showcase some of Liferay's UI taglibs.
<br />
<br />

<a href="<portlet:renderURL><portlet:param name="mvcPath" value="/html/calendar/calendar.jsp" /></portlet:renderURL>">liferay-ui:calendar</a>
<br />
<br />

<a href="<portlet:renderURL><portlet:param name="mvcPath" value="/html/calendar/tabs.jsp" /></portlet:renderURL>">liferay-ui:tabs</a>
<br />
<br />

<a href="<portlet:renderURL><portlet:param name="mvcPath" value="/html/calendar/toggle.jsp" /></portlet:renderURL>">liferay-ui:toggle</a>
<br />
<br />

<a href="<portlet:renderURL><portlet:param name="mvcPath" value="/html/wap/photo_gallery/view.jsp" /></portlet:renderURL>">wap-photo-gallery</a>
<br />
<br />

<a href="<portlet:renderURL><portlet:param name="mvcPath" value="/html/wap/view/view.jsp" /></portlet:renderURL>">wap-video</a>
<br />
<br />