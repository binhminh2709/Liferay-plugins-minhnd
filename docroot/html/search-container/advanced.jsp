<%@ include file="/init.jsp"%>

<%
  PortletURL portletURL = renderResponse.createRenderURL();
  String path = Constants.PATH_SEARCH_CONTAINER;
  
  portletURL.setParameter("mvcPath", path + "advanced.jsp");
%>
<liferay-ui:header backURL="<portlet:renderURL />" title="Detail User List" />

<liferay-ui:search-container delta="<%=5%>" headerNames="email-address,screen-name,user-id"
  iteratorURL="<%=portletURL%>">
  <liferay-ui:search-container-results>
    <%
      results = UserLocalServiceUtil.getUsers(searchContainer.getStart(), searchContainer.getEnd());
          total = UserLocalServiceUtil.getUsersCount();
          
          pageContext.setAttribute("results", results);
          pageContext.setAttribute("total", total);
    %>
  </liferay-ui:search-container-results>

  <liferay-ui:search-container-row className="com.liferay.portal.model.User" escapedModel="<%=true%>"
    keyProperty="userId" modelVar="curUser">
    <liferay-portlet:renderURL varImpl="rowURL" windowState="<%=WindowState.MAXIMIZED.toString()%>">
      <portlet:param name="mvcPath" value="/html/taglib/advanced_user_display.jsp" />
      <portlet:param name="redirect" value="<%=currentURL%>" />
      <portlet:param name="userId" value="<%=String.valueOf(curUser.getUserId())%>" />
    </liferay-portlet:renderURL>

    <liferay-ui:search-container-row-parameter name="rowURL" value="<%=rowURL.toString()%>" />

    <liferay-ui:search-container-column-jsp align="left" path="/html/taglib/advanced_column.jsp" />

    <liferay-ui:search-container-column-text buffer="buffer" name="email-address">
      <%
        buffer.append("<a href=\"");
              buffer.append(rowURL.toString());
              buffer.append("\">");
              buffer.append(curUser.getEmailAddress());
              buffer.append("</a>");
      %>
    </liferay-ui:search-container-column-text>

    <liferay-ui:search-container-column-text href="<%=rowURL%>" name="screen-name" property="screenName" />

    <liferay-ui:search-container-column-text href="<%=rowURL%>" name="user-id"
      value="<%=String.valueOf(curUser.getUserId())%>" />
  </liferay-ui:search-container-row>

  <liferay-ui:search-iterator />
</liferay-ui:search-container>

<div class="separator"></div>

<a href="<portlet:renderURL />">&laquo; Back</a>